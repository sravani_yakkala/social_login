
## Instructions

To install this module on your computer,  

````install mongodb on your computer.```` 
                    
clone and and install npm dependencies.  

````
$ git clone git@bitbucket.org:sravani_yakkala/social_login.git

$ cd social_login

$ npm install

update mongodb details in config/config.js

$ node server.js

````

Open a web browser and navigate to [http://localhost:8080/](http://localhost:8080/)

## Note..

check ``Docs`` directory for mongodb schema.
 