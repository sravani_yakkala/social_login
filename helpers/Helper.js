"use strict";
const tracer = require('tracer');

class Helper {
    constructor() {
        this.logger = null;
    }

    getLogger() {
        if (this.logger) {
            return this.logger;
        }
        this.logger = tracer.console({
            format: "{{timestamp}} [{{title}}] {{message}} (in {{path}}:{{line}})",
            dateformat: "dd-mm-yyyy HH:MM:ss TT"
        });
        return this.logger;
    }
}

Helper.shared = new Helper();

module.exports = Helper;

