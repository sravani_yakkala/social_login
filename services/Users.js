"use strict";
const callbacks = require("../helpers/Callbacks").shared;
const errors = require("../helpers/Errors");

class Users {
    constructor() {
        this.fields = {
            "_id": "_id",
            "emailId": "emailId"
        };
        this.collection = "known_users"
    }

    isKnown(emailId, mongoDb) {
        let me = this;
        return new Promise(async function (resolve) {
            try {
                let queryObj = {};
                queryObj[me.fields.emailId] = emailId;
                let projection = {
                    fields: {}
                };
                projection.fields[me.fields._id] = 0;
                console.log(queryObj,"*****")
                let itemsArray = await mongoDb.collection(me.collection).find(queryObj, projection).limit(1).toArray();
                if (!itemsArray || itemsArray.length === 0) {
                    return callbacks.fail(resolve, null, errors.UNKNOWN_USER)
                }
                callbacks.success(resolve, true)
            } catch (e) {
                callbacks.fail(resolve, new Error(e), errors.INTERNAL_ERROR)
            }
        })
    }

    whiteList(emailId, mongoDb) {
        let me = this;
        return new Promise(async function (resolve) {
            try {
                let insertObj = {};
                insertObj[me.fields.emailId] = emailId;
                await mongoDb.collection(me.collection).insert(insertObj);
                callbacks.success(resolve)
            } catch (e) {
                callbacks.fail(resolve, new Error(e), errors.INTERNAL_ERROR)
            }
        })
    }
}

Users.shared = new Users();
module.exports = Users;