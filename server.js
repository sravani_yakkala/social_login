const config = require("./config/config")

var express = require('express');
var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth20').Strategy;

const users = require("./services/Users").shared;
const helper = require("./helpers/Helper").shared;
const logger = helper.getLogger();

const MongoClient = require('mongodb').MongoClient;
const errors = require("./helpers/Errors");
const jwt = require('jsonwebtoken');
let mongoDb;

// authentication
passport.use(new GoogleStrategy({
        clientID: config.GOOGLE.CLIENT_ID,
        clientSecret: config.GOOGLE.CLIENT_SECRET,
        callbackURL: config.GOOGLE.CALLBACK_URL
    },
    async function (accessToken, refreshToken, profile, cb) {
        console.log(profile);
        if (!profile.emails || profile.emails.length === 0) {
            return cb(errors.UNABLE)
        }
        let user = {
            username: profile.name.givenName,
            displayName: profile.name.givenName,
            email: profile.emails[0].value
        };
        {
            const {ok, data, error, display} = await users.isKnown(user.email, mongoDb);
            if (!ok) {
                if (!display) {
                    return cb(error);
                }
                return cb(display.desc)
            }
        }
        return cb(null, user)
    }
));


passport.serializeUser(function (user, cb) {
    cb(null, user);
});

passport.deserializeUser(function (obj, cb) {
    cb(null, obj);
});


// Create a new Express application.
var app = express();
// Configure view engine to render EJS templates.
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

// Use application-level middleware for common functionality, including
// logging, parsing, and session handling.
app.use(require('morgan')('combined'));
app.use(require('cookie-parser')());
app.use(require('body-parser').urlencoded({extended: true}));
app.use(require('express-session')({secret: 'keyboard cat', resave: true, saveUninitialized: true}));

// Initialize Passport and restore authentication state, if any, from the
// session.
app.use(passport.initialize());
app.use(passport.session());


async function init() {
    try {
        let url = `mongodb://${config.mongo.host}:${config.mongo.port}/${config.mongo.db}`;
        let db = await MongoClient.connect(url, {useNewUrlParser: true, useUnifiedTopology: true});
        mongoDb = db.db(config.mongo.db);
    } catch (e) {
        console.error(e);
        throw new Error(e);
    }
    defineRoutes();
    let defaultPort = 8080;
    app.listen(process.env['PORT'] || defaultPort);
    logger.log("server is running on port",defaultPort)
}

function defineRoutes() {

    app.get('/',
        function (req, res) {
            res.render('home', {user: req.user});
        });

    app.get('/login',
        function (req, res) {
            res.render('login');
        });


    app.get('/auth/google',
        passport.authenticate("google", {scope: ['profile', 'email']}));

    app.get('/auth/google/callback',
        passport.authenticate('google', {failureRedirect: '/login'}),
        function (req, res) {
            logger.log(req.user, "******");
            // Successful authentication, redirect home.
            const token = jwt.sign({
                data: req.user.email
            }, config.server.jwtSecret, {expiresIn: '1h'});
            res.cookie('token', token);
            res.redirect('/');
        });

    app.post('/whitelist', requireToken, async function (req, res) {
        let email = req.body.email;
        const {ok, error, data, display} = await users.whiteList(email, mongoDb);
        if (!ok) {
            if (!display) {
                return internalError(res)
            }
            return res.status(display.status)
                .end(display.desc)
        }
        return res.status(200)
            .send({msg: "user whitelisted", email: email})
    });

}


function requireToken(req, res, next) {
    if (!req.cookies || !req.cookies.token) {
        return res.status(errors.UN_AUTHORIZED.status)
            .end(errors.UN_AUTHORIZED.desc)
    }
    const token = req.cookies.token;
    try {
        const decoded = jwt.verify(token, config.server.jwtSecret);
    } catch (err) {
        logger.warn("invalid token", token);
        return res.status(errors.UN_AUTHORIZED.status)
            .end(errors.UN_AUTHORIZED.desc)
        // err
    }
    next();
}

function internalError(res) {
    res.status(errors.INTERNAL_ERROR.status)
        .end(errors.INTERNAL_ERROR.desc)
}

init();
